Setup Gitlab with CI - Monitoring and Automatic staging environment
--------------------------------

# Setup

## Requirement

  - Docker 1.13+
  - docker-machine && docker-compose
  - Virtualbox
  - OSX


## Quick-start

If you want to run it directly, you can bootstrap everything with:

```bash
# ./up <My network ip>
$ ./up 10.3.4.5
```

## Create your Swarm local cluster

```bash
docker-machine create manager1
eval $(docker-machine env manager1)
docker swarm init --advertise-addr `docker-machine ip manager1`
docker-machine create worker1
eval $(docker-machine env worker1)
docker swarm join  \
   --token `docker-machine ssh manager1 docker swarm join-token worker -q`  \
   `docker-machine ip manager1`:2377
docker-machine create worker2
eval $(docker-machine env worker2)
docker swarm join  \
   --token `docker-machine ssh manager1 docker swarm join-token worker -q`  \
   `docker-machine ip manager1`:2377
```
## Setup Dns

```bash
# run bind/startup.sh like this
# LOCAL_IP=<My network ip> sh ./startup.sh <swarm master docker-machine name>
cd bind
$ LOCAL_IP=10.3.4.5 sh ./startup.sh manager1
```

## Run Full stack

```bash
$ sh ./startup.sh
```

# Access

You can access the different service there:

  - [Gitlab](http://gitlab.lam.local) (root/jgsqware)
  - [Mattermost](http://mattermost.lam.local) (root/jgsqware)
  - [Prometheus](http://metrics.lam.local)
  - [Grafana](http://grafana.lam.local) (admin/admin)
  - [Artifactory](http://artifactory.lam.local)



mkdir -p /etc/gitlab/ssl
chmod 700 /etc/gitlab/ssl
openssl req -nodes -newkey rsa:2048 -keyout /etc/gitlab/ssl/gitlab.lam.local.key -out /etc/gitlab/ssl/gitlab.lam.local.csr
openssl x509 -req -days 1460 -in /etc/gitlab/ssl/gitlab.lam.local.csr -signkey /etc/gitlab/ssl/gitlab.lam.local.key -out /etc/gitlab/ssl/gitlab.lam.local.crt
rm -v /etc/gitlab/ssl/gitlab.lam.local.csr
chmod 600 /etc/gitlab/ssl/gitlab.lam.local.*