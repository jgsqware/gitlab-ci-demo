#!/bin/bash -eu

if [ ! -z $1 ]; then
  SWARMNAME=$1
  echo "Using Swarm manager: $SWARMNAME"
fi

sh ./clean.sh
eval $(docker-machine env -u)
docker-compose down -v
docker-compose up -d --build

# Wait for gitlab is booted and Go to http://gitlab.lam.local and setup default password to jgsqware
echo "Gitlab is booting..."; while [ "healthy" != $(docker inspect -f "{{.State.Health.Status}}" local_gitlab.lam.local_1) ]; do sleep 1; done;
docker-compose exec gitlab-runner gitlab-runner register -n \
  --url http://gitlab.lam.local/ci \
  --registration-token EzPNSEZFVAEHgFPhWXxK \
  --executor docker \
  --description "My Docker Swarm Runner" \
  --docker-image "docker:latest" \
  --docker-volumes $HOME/.docker:/.docker

docker-compose exec gitlab-runner gitlab-runner register -n \
  --url http://gitlab.lam.local/ci \
  --registration-token EzPNSEZFVAEHgFPhWXxK \
  --executor docker \
  --description "My Docker Swarm Runner for build" \
  --docker-image "docker:latest" \
  --docker-volumes /var/run/docker.sock:/var/run/docker.sock

# docker-compose exec --user git gitlab git clone --bare https://github.com/jgsqware/example-voting-app.git /var/opt/gitlab/git-data/repositories/root/example-voting-app.git
docker-compose exec --user git gitlab.lam.local git clone --bare https://github.com/jenkins-demo-org/example-voting-app.git /var/opt/gitlab/git-data/repositories/root/example-voting-app.git
docker-compose exec --user git gitlab.lam.local git clone --bare https://github.com/jenkins-demo-org/worker.git /var/opt/gitlab/git-data/repositories/root/worker.git
docker-compose exec --user git gitlab.lam.local gitlab-rake gitlab:import:repos

SWARM_IP=$(docker-machine ip $SWARMNAME)
if [ $? == 1 ]; then
  echo 'Swarm not enabled...'
else
  echo 'Swarm enabled...'
  PRIVATE_TOKEN=$(curl http://gitlab.lam.local/api/v3/session --data 'login=root&password=jgsqware' | jq '.private_token')
  PRIVATE_TOKEN=$(sed -e 's/^"//' -e 's/"$//' <<<"$PRIVATE_TOKEN")
  curl --request POST --header "PRIVATE-TOKEN: $PRIVATE_TOKEN" "http://gitlab.lam.local/api/v4/projects/1/variables" --form "key=MANAGER_IP" --form "value=$SWARM_IP" 
fi


