#!/bin/bash
set -e

echo $(pwd)
eval $(docker-machine env -u)
docker-compose down -v

DATA=$(ls ./data)

for d in ${DATA[@]}
do
  echo "remove data from ./data/$d/*"
  rm -rf ./data/$d/*
done

