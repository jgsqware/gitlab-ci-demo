#!/bin/bash

echo "Cleaning bind $(pwd)"

docker-machine rm -f bind
DNS=$(networksetup -getdnsservers Wi-Fi | grep --invert "192.168.99")
networksetup -setdnsservers Wi-Fi $DNS
rm -fr ./data