#!/bin/bash

DNS=$(networksetup -getdnsservers Wi-Fi | grep --invert "192.168.99")
if [[ $DNS == *"Wi-Fi"* ]]; then
  networksetup -setdnsservers Wi-Fi "8.8.8.8"
else
  networksetup -setdnsservers Wi-Fi $DNS
fi

docker-machine stop "bind"