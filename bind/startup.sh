#!/bin/bash
eval $(docker-machine env -u)

if [ -z $LOCAL_IP ]; then
  echo 'set $LOCAL_IP'
  exit 1
fi 

USE_SWARM=0
if [ -z $1 ]; then
  echo 'Swarm not enabled...'
else
  echo 'Swarm enabled...'
  USE_SWARM=1
fi

SWARMNAME="$1"
MACHINENAME="bind"

if [[ $USE_SWARM == 1 ]]; then

  SWARM_IP=$(docker-machine ip $SWARMNAME)

  if [ $? == 1 ]; then
    echo 'docker-machine $SWARMNAME is not running'
    exit 1
  fi
fi

MACHINE_STATUS=$(docker-machine status $MACHINENAME)

if [ $? == 1 ]; then
  echo "create $MACHINENAME docker-machine"
  docker-machine create -d virtualbox $MACHINENAME
elif [ "Running" != $MACHINE_STATUS ]; then
  docker-machine start $MACHINENAME
fi

echo "connect to $MACHINENAME"
eval $(docker-machine env $MACHINENAME)

echo "update conf"

mkdir -p ./data
rm -rf ./data/bind

tar xvf bind-data.tar -C ./data

OLD_SWARM_IP="{{OLD_SWARM_IP}}"
ORIGINAL_LOCAL_IP="{{ORIGINAL_LOCAL_IP}}"
ORIGINAL_IP="{{ORIGINAL_IP}}"
INVERTED_ORIGINAL_IP="{{INVERTED_ORIGINAL_IP}}"
NEW_IP=$(docker-machine ip $MACHINENAME)

IFS=. read ip1 ip2 ip3 ip4 <<< "$NEW_IP"
INVERTED_NEW_IP="$ip4.$ip3.$ip2.$ip1"

sed -i .bak "s/$INVERTED_ORIGINAL_IP/$INVERTED_NEW_IP/" data/bind/etc/named.conf.local
sed -i .bak "s/$ORIGINAL_IP/$NEW_IP/" data/bind/etc/named.conf.local

mv "data/bind/lib/REV_TEMPLATE.rev" "data/bind/lib/$NEW_IP.rev"
sed -i .bak "s/$INVERTED_ORIGINAL_IP/$INVERTED_NEW_IP/" "data/bind/lib/$NEW_IP.rev"
rm data/bind/lib/$NEW_IP.rev.bak

sed -i .bak "s/$ORIGINAL_IP/$NEW_IP/; s/$ORIGINAL_LOCAL_IP/$LOCAL_IP/; s/$OLD_SWARM_IP/$SWARM_IP/" data/bind/lib/*.local.hosts

echo "Removing *.bak file"
find ./data/bind -name "*.bak" -type f -delete 

echo "Add dns for OSX for WiFi"
DNS=$(networksetup -getdnsservers Wi-Fi | grep --invert "192.168.99")
if [[ $DNS == *"Wi-Fi"* ]]; then
  networksetup -setdnsservers Wi-Fi $NEW_IP "8.8.8.8"
else
  networksetup -setdnsservers Wi-Fi $NEW_IP $DNS
fi

echo "instanciate bind"
docker-compose down -v
docker-compose up -d

echo "Your DNS is running on $NEW_IP"