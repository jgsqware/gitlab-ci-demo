#!/bin/bash

eval $(docker-machine env -u)
docker-machine create manager1
eval $(docker-machine env manager1)
docker swarm init --advertise-addr `docker-machine ip manager1`
docker-machine create worker1
eval $(docker-machine env worker1)
docker swarm join  \
   --token $(docker-machine ssh manager1 docker swarm join-token worker -q)  \
   `docker-machine ip manager1`:2377
docker-machine create worker2
eval $(docker-machine env worker2)
docker swarm join  \
   --token $(docker-machine ssh manager1 docker swarm join-token worker -q)  \
   `docker-machine ip manager1`:2377

eval $(docker-machine env manager1)
docker network create --driver=overlay traefik-net
docker stack deploy --compose-file traefik/docker-traefik.yml traefik